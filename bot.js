var env = require('node-env-file');
env(__dirname + '/.env');

var Botkit = require('botkit');
var debug = require('debug')('botkit:main');

var luis = require('./luis-middleware.js');
var request = require('request');

var bot_options = {
    replyWithTyping: true,
};

var luisAppUri='https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/052ff3c1-9312-42df-a15b-c8dde5284b2c?subscription-key=d823d554873545b097706c82b0bee690&verbose=true&timezoneOffset=0&q=';
var weatherUrl = 'http://api.openweathermap.org/data/2.5/forecast?APPID=a7f3a2fbef1a37b8312bbd39eacca2b5&q=';

var luisOptions = {serviceUri: luisAppUri};

// Use a mongo database if specified, otherwise store in a JSON file local to the app.
// Mongo is automatically configured when deploying to Heroku
if (process.env.MONGO_URI) {
  // create a custom db access method
  var db = require(__dirname + '/components/database.js')({});
  bot_options.storage = db;
} else {
    bot_options.json_file_store = __dirname + '/.data/db/'; // store user data in a simple JSON format
}

// Create the Botkit controller, which controls all instances of the bot.
var controller = Botkit.socketbot(bot_options);

// Set up an Express-powered webserver to expose oauth and webhook endpoints
var webserver = require(__dirname + '/components/express_webserver.js')(controller);


controller.middleware.receive.use(luis.middleware.receive(luisOptions));

controller.hears(['Begrüßung'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    bot.reply(message,"Hallo, ich bin ein TestBot von eforce21!");
});

controller.hears(['Vorstellung'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    var personName = '';
    for(var i=0; i<message.entities.length; ++i) {
        var e = message.entities[i];

        if(e.type === "PersonenName") {
            personName = e.entity.charAt(0).toUpperCase() + e.entity.substr(1);
            break;
        }
    }
    var text = "Hallo ";
    if(personName.length > 0) {
        text += personName +", "
    }
    var responses = ['schön, dich kennen zu lernen', 'freut mich, dich kennen zu lernen', 'wie kann ich behilflich sein?', 'ich bin ein TestBot von eforce21!'];
    text += responses[Math.floor(Math.random() * Math.floor(responses.length))];

    bot.reply(message,text);
});

controller.hears(['Wetterbericht'],'message_received', luis.middleware.hearIntent, function(bot, message) {

    var city = "muenchen";
    for(var i=0; i<message.entities.length; ++i) {
        var e = message.entities[i];

        if(e.type === "Stadt") {
            city = e.entity;
            break;
        }
    }

    //Replace Umlauts
    city = city.replace(/[\u00e4|\u00c4]/g, "ae");
    city = city.replace(/[\u00dc|\u00fc]/g, "ue");
    city = city.replace(/[\u00d6|\u00f6]/g, "oe");
    city = city.replace(/[\u00df]/g, "ss");

    var url = weatherUrl + city;

    request.get(url, function(error, response, body) {
        try {
            var parsed = JSON.parse(body);

            var cityname = parsed.city.name;
            var first_prediction = parsed.list[0];

            var first_time = first_prediction.dt_txt;
            var min_temp = first_prediction.main.temp_min - 273.15;
            min_temp = min_temp.toFixed(1) + "°C";
            var max_temp = first_prediction.main.temp_max - 273.15;
            max_temp = max_temp.toFixed(1)+ "°C";
            var humidity = first_prediction.main.humidity + "%";
            var pressure = first_prediction.main.pressure + " hpa";

            var cloudiness = first_prediction.weather[0].description;

            var answerstring = "Das Wetter in " + cityname + " hat um " + first_time + " Temperaturen zwischen "
                + min_temp + " und " + max_temp + " bei einer Luftfeuchtigkeit von " + humidity + " und Luftdruck von "
                + pressure + ". Der Himmel zeigt " + cloudiness + ".";

            bot.reply(message, answerstring);
        } catch (e) {
            bot.reply(message, "Ich weiß das Wetter in " + city +" leider nicht.");
        }
    });

});

controller.hears(['eforce21 info'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    bot.reply(message,"eforce21 - digital Architects liefern Maßgeschneiderte IT-Systeme, Agile Software-Entwicklung und erstklassiges Design.");
});

controller.hears(['Uhrzeit'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    bot.reply(message,"Das Datum und die Uhrzeit sind " + new Date().toLocaleString());
});


controller.hears(['None'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    var responses = ['Mhm', 'Okay.', 'Cool.', 'OK', 'Aha.', 'Soso'];
    bot.reply(message, responses[Math.floor(Math.random() * Math.floor(responses.length))]);
});


//TODO train better
/*
controller.hears(['Verabschiedung'],'message_received', luis.middleware.hearIntent, function(bot, message) {
    bot.reply(message,"Auf Wiedersehen!");
    //process.exit();
});
*/

controller.on('conversationStarted', function(bot, convo) {
    console.log('Conversation started with', convo.context.user);
});

// Open the web socket server
controller.openSocketServer(controller.httpserver);
controller.startTicking();

//TODO solve differently
/*
controller.spawn({}, function(bot) {
    bot.say({text: "Hallo Welt. Ich bin ein TestBot von eforce21"});
    bot.say({text: "Du kannst mir z.B. deinen Namen sagen, oder nach dem Wetter oder der Uhrzeit fragen, oder nach mehr infos über eforce21."});
});
*/


console.log('Bot online at: http://localhost:' + (process.env.PORT || 3000));
